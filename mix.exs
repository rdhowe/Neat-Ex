defmodule Neat.Mixfile do
  use Mix.Project

  def project do
    [
      app: :neat_ex,
      version: "1.1.1",
      elixir: "~> 1.2",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      description: description,
      package: package,
      deps: deps,
      name: "Neat-Ex",
      source_url: "https://gitlab.com/onnoowl/Neat-Ex",
      docs: [main: Neat]
    ]
  end

  defp deps do
    [
      { :json,   "~> 0.3.0"},
      {:earmark, "~> 0.1", only: :dev},
      {:ex_doc,  "~> 0.10", only: :dev}
    ]
  end

  defp description do
    """
    This project provides the means to define, simulate, and serialize Artificial-Neural-Networks (ANNs), as well as the means to develop them through use of the Neuro-Evolution of Augmenting Toplogies (NEAT) algorithm created by Dr. Kenneth Stanley.

    Neuro-Evolution, unlike back-propogation, easily allows the usage of recurrent neural networks instead of just feed-forward networks, and fitness functions instead of just training data. Additionally, since NEAT augments topologies, all the engine needs to start is the input/output layout, and a fitness function.
    """
  end

  defp package do
    [
      maintainers: ["Stuart Hunt"],
      licenses: ["MIT License"],
      links: %{"GitLab" => "https://gitlab.com/onnoowl/Neat-Ex"}
    ]
  end
end

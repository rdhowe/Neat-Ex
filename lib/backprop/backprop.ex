defmodule Backprop do
  @moduledoc """
  This module provides the means for backpropogation.

  ## Example Usage:
      iex> ann = Ann.newFeedforward([1, 2], [3], [3, 3]) #Neurons 1 and 2 are inputs, neuron 3 is output, and there are two layers of 3 hidden neurons.
      iex> patterns = [
      ...>   { %{1 => 0.0, 2 => 1.0},  %{3 => 1.0} },
      ...>   { %{1 => 1.0, 2 => 0.0},  %{3 => 1.0} },
      ...>   { %{1 => 0.0, 2 => 0.0},  %{3 => 0.0} },
      ...>   { %{1 => 1.0, 2 => 1.0},  %{3 => 0.0} },
      ...> ]
      iex> {_ann, info} = Backprop.train ann, fn _ -> Enum.take_random(patterns, 2) end
      iex> IO.puts "Steps taken: \#{info.step}"
      :ok

  | Option       | Default                  | Description |
  | ------------ | ------------------------ | ----------- |
  | learn_val    | 5.0                      | Affects learning speed. |
  | terminate?   | fn _ann, info -> info.step > 50_000 end | Takes 2 arguments, the latest ann (or sim), and the info map. If the function returns true, training is terminated. |
  | sigmoid      | &Backprop.sigmoid/1      | The sigmoid function used. |
  | sigmoidPrime | &Backprop.sigmoidPrime/1 | The derivative of the sigmoid function used. |
  | worker_count | 1                        | Number of workers for computing batch elements independently. (Should always be less than batch size.) |

  The `info` map is a map of miscellaneous info with the keys [:step, :adjError]. Step goes up once per iteration, and adjError is an adjusted and smoothed form of the error evaluated through backpropogation. These two values are intended for use in the :terminate? function, possibly for determining what quantity of patterns to train with at a given time, and for debugging.
  """

  @doc "Populates the given map with the default options used in the Backprop module. Throws an error when given an invalid option. See the module description for details on available options."
  def fillDefaults!(opts) do
    defaults = %{worker_count: 1, learn_val: 5.0, terminate?: fn _ann, info -> info.step > 50_000 end, sigmoid: &sigmoid/1, sigmoidPrime: &sigmoidPrime/1}
    Enum.each opts, fn {k, _v} ->
      if !Map.has_key?(defaults, k), do: raise "Option \"#{k}\" is not recognized. Here are available options: #{inspect Map.keys(defaults)}"
    end
    Map.merge(defaults, opts)
  end

  @doc "Returns `{ann, info}`. Takes a neural network (preferably with randomized weights), a function that takes one argument (the `info` map) and returns a list of training patterns, and an optional map of options (see module description for option details and details on `info`)."
  def train(ann, patternFun, opts \\ %{}) do
    terminateDefined = Map.has_key?(opts, :terminate?)
    #uses trainSim, then applies the sim's weightMap back onto the ANN.
    opts = fillDefaults!(opts)
    if terminateDefined, do: opts = Map.put(opts, :terminate?, fn sim, info -> opts.terminate?.(Ann.Simulation.exportAnn(sim), info) end)

    {sim, info} = trainSim Ann.Simulation.new(ann, opts.sigmoid), patternFun, opts

    {Ann.Simulation.exportAnn(sim), info}
  end

  @doc "Takes a prepared simulation (for the purposes of collecting neuron dependency information), a patternFun, and an option map (see &Backprop.train/1 for details)."
  def trainSim(sim, patternFun, opts, info \\ %{step: 0, adjError: 1.0}) do
    patterns = patternFun.(info)
    batch_size = length(patterns)

    {grad_sum, error_sum} = patterns
      |> Async.worker_map(opts.worker_count, fn {inputs, outputs} ->
        getGradient(sim, inputs, outputs, opts) #collects the gradients using workers
      end)
      |> Enum.reduce({%{}, 0}, fn {gradient, error}, {grad_sum, error_sum} ->
        {
          Map.merge(grad_sum, gradient, fn _conn_id, v1, v2 -> (v1 + v2) / batch_size end),
          error_sum + error
        }
      end) #sums the gradient vectors

    weightMap = Map.merge(sim.weightMap, grad_sum, fn _conn_id, weight, gradientComponent ->
      weight - opts.learn_val*gradientComponent
    end) #adds the gradient vector onto the weights to make an updated weightMap.
    sim = Map.put(sim, :weightMap, weightMap)

    adjError = 0.75*info.adjError + 0.25*(error_sum/batch_size) #updates the smoothed adjError.
    if opts.terminate?.(sim, info) do
      {sim, info}
    else
      trainSim(sim, patternFun, opts,
        info |> Map.put(:step, info.step + 1) |> Map.put(:adjError, adjError)
      )
    end
  end

  @doc "Gets the gradient vector (as a map with format %{conn_id => grad_value}) for a single training pattern using backpropogation. Returns {gradient_map, error_sum}."
  def getGradient(sim, inputs, outputs, opts) do
    inputs = Enum.reduce(sim.ann.bias, inputs, fn bia, inputs ->
      Map.put(inputs, bia, 1)
    end) #adds the biases as inputs.

    feedforward_map = Enum.reduce inputs, %{}, fn {id, val}, dict ->
      Map.put(dict, id, {0, val}) #tuple is {left, right} following the back-propogation algorithm.
    end #initializes feedforward_map with input data.

    {feedforward_map, backprop_map, error_sum} = Enum.reduce(outputs, {feedforward_map, %{}, 0},
      fn {id, val}, {feedforward_map, backprop_map, error_sum} ->
        {{left, right}, feedforward_map} = getFeedforward(id, feedforward_map, sim, opts)
        {
          feedforward_map,
          Map.put(backprop_map, id, left*(right - val)),
          error_sum + 0.5*:math.pow(right - val, 2)
        }
      end
    ) #evaluates the feedforward stage, and preps a mostly empty backprop_map for the backpropogation stage.

    backprop_map = Enum.reduce(inputs, backprop_map,
      fn {id, _val}, backprop_map ->
        {_backprop_error, backprop_map} = getBackprop(id, backprop_map, feedforward_map, sim)
        backprop_map
      end
    ) #fleshes out the backprop_map from the starting values.

    gradient = Enum.reduce sim.ann.connections, %{}, fn {conn_id, conn}, gradient ->
      {_left, right} = Map.fetch!(feedforward_map, conn.input)
      gradientComponent = right*Map.fetch!(backprop_map, conn.output)
      Map.put(gradient, conn_id, gradientComponent)
    end #creates the gradient vector from the feedforward_map and backprop_map.

    {gradient, error_sum}
  end

  @doc "Fetches a value from the feedforward_map if it exists, and if not it will update the feedforward_map. Returns {{left, right}, feedforward_map}. Left and right are the left/right hand values according to the back-propogation algorithm found [here](http://page.mi.fu-berlin.de/rojas/neural/chapter/K7.pdf). The right side is the standard neuron value.\nThis evaluates the feedforward portion of the back-propogation algorithm, only it does it backwards and recursively for the purposes of dynamic and efficient dependency management, meaning this will work just as well for abnormal topologies. It starts at an output, evaluates dependencies, traces everything back to the inputs via recursion, and then unfolds to evaluate the feedforward left/right values. The feedforward_map contains all of this collected data. This method is done so that when fetching the feedforward data, it is either retrieved immediately, or recursively evaluated. This avoids repeating computations."
  def getFeedforward(id, feedforward_map, sim, opts) do
    val = Map.get(feedforward_map, id)
    if val do
      {val, feedforward_map}
    else
      {feedforward_map, sum} = Enum.reduce(Map.fetch!(sim.deps, id), {feedforward_map, 0},
        fn {dep_id, conn_id}, {feedforward_map, sum} ->
          {{_left, right}, feedforward_map} = getFeedforward(dep_id, feedforward_map, sim, opts)
          {feedforward_map, sum + (right * Map.fetch!(sim.weightMap, conn_id))}
        end
      )
      val = {opts.sigmoidPrime.(sum), opts.sigmoid.(sum)}
      {val, Map.put(feedforward_map, id, val)}
    end
  end

  @doc "Fetches a value from the backprop_map if it exists, and if not it will update the backprop_map. Returns {backprop_error, backprop_map}. Uses a similar strategy to getFeedforward in it evaluates forwards and recursively (instead of backwards) to evaluate dependencies, and then unfolds (which goes backwards)."
  def getBackprop(id, backprop_map, feedforward_map, sim) do
    val = Map.get(backprop_map, id)
    if val do
      {val, backprop_map}
    else
      {sum, backprop_map} = Enum.reduce(Map.fetch!(sim.affects, id), {0, backprop_map},
        fn {affect_id, conn_id}, {sum, backprop_map} ->
          {backprop_error, backprop_map} = getBackprop(affect_id, backprop_map, feedforward_map, sim)
          {sum + (backprop_error * Map.fetch!(sim.weightMap, conn_id)), backprop_map}
        end
      )
      {left, _right} = Map.fetch!(feedforward_map, id)
      backprop_error = left*sum
      {backprop_error, Map.put(backprop_map, id, backprop_error)}
    end
  end

  @doc "The sigmoid function suggested for use in this backpropogation implementation. The c parameter affects the steepness of the sigmoid."
  def sigmoid(x, c \\ 3), do: 1 / (1 + :math.exp(-c * x))
  @doc "The derivative of the suggested sigmoid function. The c parameter affects the steepness of the sigmoid."
  def sigmoidPrime(x, c \\ 3), do: sigmoid(x, c) * (1 - sigmoid(x, c))

  @doc "Handy math formula. When `x` is `0` the return value will be close to `from`. When `x` is `width` the return value will be close to `to`. In between the graph appears like a sigmoid providing gentle asymptoting at either end."
  def customSigmoid(x, from, to, width), do: (to-from) / (1 + :math.exp(4 * (1 - 2*x/width))) + from
end

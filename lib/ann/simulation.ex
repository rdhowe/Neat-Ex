defmodule Ann.Simulation do
  @moduledoc "A module for simulating ANNs."
  defstruct ann: nil, step: 0, data: %{}, evaluating: MapSet.new, previous: %{}, deps: nil, affects: nil, affectSet: nil, weightMap: nil, sigmoid: nil, maxed: false

  alias Ann.Simulation

  @doc "Creates a new ANN Simulation given an ANN, and a sigmoid function."
  def new(ann, sigmoid \\ fn x -> :math.tanh(2*x) end) do
    {deps, affects, affectSet} = ann.connections
      |> Enum.filter(fn {_id, conn} -> conn.enabled end)
      |> Enum.reduce({%{}, %{}, %{}},
        fn {conn_id, conn}, {deps, affects, affectSet} ->
          {input_id, output_id} = {{conn.input, conn_id}, {conn.output, conn_id}}
          {
            Map.update(deps, conn.output, [input_id], &[input_id | &1]),
            Map.update(affects, conn.input, [output_id], &[output_id | &1]),
            Map.update(affectSet, conn.input, MapSet.put(MapSet.new, conn.output), &MapSet.put(&1, conn.output))
          }
        end)
    affectSet = Map.put(affectSet, :input,
      Enum.reduce(ann.input, MapSet.new,
        fn input, acc ->
          MapSet.union(acc, Map.get(affectSet, input, MapSet.new))
        end
      )
    ) #affectSet[:input] is a Set of all nodes affected by a change in input.

    weightMap = ann.connections
      |> Enum.filter(fn {_id, conn} -> conn.enabled end)
      |> Enum.reduce(%{},
        fn {conn_id, conn}, weightMap ->
          Map.put(weightMap, conn_id, conn.weight)
        end)

    data = Enum.reduce(ann.bias, %{}, fn bia, dict ->
        Map.put(dict, bia, 1)
      end)

    %Simulation{ann: ann, deps: deps, data: data, affects: affects, affectSet: affectSet, weightMap: weightMap, sigmoid: sigmoid}
  end

  @doc "Applies the weightMap back onto the ANN and returns it. Note, this is only of use if the stored weightMap has been modified manually or by an external source such as the Backprop or NeuralTrainer modules."
  def exportAnn(sim) do
    Map.put(sim.ann, :connections, Enum.reduce(sim.weightMap, sim.ann.connections, fn {conn_id, weight}, conns ->
      Map.update!(conns, conn_id, fn conn ->
        Map.put(conn, :weight, weight)
      end)
    end))
  end

  @doc "Steps the simulation forward one frame, provided with the current inputs for this frame. inputs should be a map of node_ids and their values."
  def step(sim, inputs) do
    previous = Map.merge(sim.data, inputs)
    evaluating = if Map.size(inputs) == 0, do: sim.evaluating, else: MapSet.union(sim.evaluating, sim.affectSet[:input])
    #Iterate through all nodes we should update, given an
    #accumulator of the {ANN data (with input values merged in), and a set of neurons to update next}
    {current, evaluating_next} = Enum.reduce(evaluating, {previous, MapSet.new},
      fn node, {data, evaluating_next} ->
        {
          Map.put(data, node,
            sim.sigmoid.(
              Enum.reduce(Map.get(sim.deps, node, []), 0,
                fn {dep, conn_id}, sum ->
                  sum + (Map.get(previous, dep, 0) * Map.fetch!(sim.weightMap, conn_id)) #gets old data from `sim.data` instead of `data`.
                end
              )
            )
          ),
          MapSet.union(evaluating_next, Map.get(sim.affectSet, node, MapSet.new))
        }
      end
    )
    %{sim | step: sim.step + 1, data: current, evaluating: evaluating_next, previous: sim.data}
    #previous is only used for network-normalization-analysis, in function `change`.
  end

  @doc "Returns the ammount of data change in the latest step of the simulation."
  def change(sim) do
    Enum.reduce sim.data, 0, fn {k, v}, sum ->
      sum + abs(v - Map.get(sim.previous, k, 0))
    end
  end

  @doc "Uses a single frame of inputs repeatedly until it has evaluated `max` times, or until the summation of data changes is <= to `thresh`."
  def eval(sim, inputs, max \\ 100, thresh \\ 0.00001)
  def eval(sim, inputs, max, thresh) when max > 0 do
    sim = step(sim, inputs)
    if change(sim) <= thresh, do: sim, else: eval(sim, %{}, max - 1, thresh)
  end
  def eval(sim, _, _, _), do: Map.put(sim, :maxed, true)
end

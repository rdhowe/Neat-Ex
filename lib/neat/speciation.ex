defmodule Neat.Speciation do
  alias Neat.Utils

  @doc "Given Neat, the compatibility_threshold is automatically modified to make the species count closer to the `target_species_number`. If there are too many species, the most similar species are merged together."
  def modify_compatibility_threshold(neat = %Neat{opts: opts}) do
    speciesNumber = length(neat.species)
    cond do
      speciesNumber < opts.target_species_number && opts.compatibility_threshold > opts.compatibility_modifier ->
        modify_compatibility_threshold(neat, -opts.compatibility_modifier)
      speciesNumber > opts.target_species_number ->
        neat
          |> modify_compatibility_threshold(opts.compatibility_modifier)
          |> Map.put(:species, mergeClosest(neat.species, opts))
      :else -> neat
    end
  end
  @doc false
  def modify_compatibility_threshold(neat, amt) do
    Map.put(neat, :opts, Map.put(neat.opts, :compatibility_threshold, neat.opts.compatibility_threshold + amt))
  end

  @doc "Given a species list, the a list of new members, and a Neat.Options, the new members are speciated. The new_members list must be structured like the species list, meaning a list of sublists. Each sublist should correspond to the species that it orginiated from. This helps, because the child will be checked with it's parent species before being compared to all other species."
  def speciate(species, new_members, opts), do: speciate(species, species, new_members, 0, opts)
  defp speciate(species, [], [], _index, _opts), do: species
  defp speciate(species, [{{rep, _, _}, _} | rest_species], [new_members | rest_new_members], index, opts) do
    species = Enum.reduce new_members, species, fn new_member, species ->
      if compatibility(opts, rep, new_member) <= opts.compatibility_threshold do
        List.update_at(species, index, fn {{rep, avg, tsi}, members} -> {{rep, avg, tsi}, [new_member | members]} end)
      else
        speciateMember(new_member, species, opts)
      end
    end
    speciate(species, rest_species, rest_new_members, index + 1, opts)
  end
  def speciateMember(ann, [], _), do: [{{ann, 0, {0, 0}}, [ann]}]
  def speciateMember(ann, [{rep, membs} | t], opts) do
    if compatibility(opts, rep, ann) <= opts.compatibility_threshold do
      [{rep, [ann | membs]} | t]
    else
      [{rep, membs} | speciateMember(ann, t, opts)]
    end
  end

  @doc "Measures the compatibility between two networks given a Neat.Options, then the two networks. Compatibility measurements will change over time to make the number of species approach opts.target_species_number."
  def compatibility(opts, ann1, ann2) do
    ann1 = Utils.deconstruct(ann1)
    ann2 = Utils.deconstruct(ann2)
    merged = Map.merge(ann1.connections, ann2.connections, fn _id, conn1, conn2 ->
      Map.put conn1, :weight, conn1.weight - conn2.weight
    end)
    avgWeightDif = (merged |> Stream.map(fn {_, conn} -> abs(conn.weight) end) |> Enum.sum) / Map.size(merged)

    set1 = getIdSet(ann1)
    set2 = getIdSet(ann2)
    differenceNum = MapSet.size(MapSet.difference(set1, set2)) + MapSet.size(MapSet.difference(set2, set1))

    opts.weight_difference_coefficient*avgWeightDif + (differenceNum * opts.structure_difference_coefficient / max(Map.size(ann1.connections), Map.size(ann2.connections)))
  end
  defp getIdSet(ann) do
    Enum.reduce Map.keys(ann.connections), MapSet.new, fn id, acc ->
      MapSet.put(acc, id)
    end
  end

  @doc "Given the species list and a Neat.Options, the two closest species (as measured by the compatibility of the representatives) are merged together."
  def mergeClosest(species, opts) do
    # List.zip([0..(length(species - 1)), species])
    {_, a, b} = Enum.flat_map(0..(length(species) - 2), fn i1 ->
      Enum.map((i1+1)..(length(species) - 1), fn i2 ->
        {{rep1, _, _}, _} = Enum.at(species, i1)
        {{rep2, _, _}, _} = Enum.at(species, i2)
        {compatibility(opts, rep1, rep2), i1, i2}
      end)
    end) |> Enum.min_by(fn {dif, _, _} -> dif end)
    {{_, _, b_tsi}, bmembers} = Enum.fetch!(species, b)
    species
      |> List.update_at(a, fn {{rep, avg, a_tsi}, amembers} -> {{rep, avg, max(a_tsi, b_tsi)}, amembers ++ bmembers} end)
      |> List.delete_at(b)
  end
end

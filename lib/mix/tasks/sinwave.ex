defmodule Mix.Tasks.Sinwave do
  def run(_) do
    sinAnn = Ann.newFeedforward([1], [2], [3, 3])

    batch_size = 2
    {time, {ann, data}} = :timer.tc(&Backprop.train/3, [sinAnn, fn data ->
      # if data.adjError < 0.001, do: batch_size = 50
      IO.write("\rstep: #{data.step}, size: #{batch_size}, adjError: #{Float.round(data.adjError, 5)}, grad_mag: #{Float.round(data.grad_mag, 5)}          ")
      Enum.map 1..batch_size, fn _ ->
        x = :rand.uniform
        {%{1 => x}, %{2 => (:math.sin(:math.pi*2*x) + 1) / 2}}
      end
    end, %{worker_count: batch_size, max_steps: 100_000, thresh: 0.0004, learn_val: 5}])
    IO.puts "\nBackpropogation took #{data.step} steps."

    Ann.saveGZ(ann, "sinNetwork.gz")

    #Graphing
    sim = Ann.Simulation.new(ann, &Backprop.sigmoid/1)
    IO.puts "-----"
    {w, h} = {77, 20}
    vals = Enum.map 0..w, fn x -> #Evaluate a sample of inputs, and map the outputs to range from -1 to 1
      Float.round(Map.fetch!(Ann.Simulation.eval(sim, [{1, x/w}], 10).data, 2), 2) * 2 - 1
    end
    Enum.each 0..h, fn y ->
      Enum.each vals, fn val ->
        adjH = -(y - h/2)/(h/2)
        IO.write(if (0 < adjH && adjH < val) || (val < adjH && adjH < 0), do: "#", else: " ")
      end
      IO.write "\n"
    end
    IO.puts "-----"

    IO.puts "Time taken: #{time / 1_000_000}s"
  end
end
